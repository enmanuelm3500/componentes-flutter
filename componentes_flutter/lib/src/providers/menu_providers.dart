import 'dart:convert';

import 'package:flutter/services.dart';

class _MenuProviders {
  List<dynamic> opciones = [];

  _MenuProviders() {}

  Future<List<dynamic>> cargarData() async {
    final resp = await rootBundle.loadString('resources/menu_opts.json');
    Map dataMap = json.decode(resp);
    opciones = dataMap['rutas'];
    return opciones;
  }
}

final menuProvider = new _MenuProviders();
