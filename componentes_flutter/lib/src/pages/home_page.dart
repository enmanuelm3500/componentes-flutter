import 'package:componentes_flutter/src/pages/alert_page.dart';
import 'package:componentes_flutter/src/providers/menu_providers.dart';
import 'package:componentes_flutter/src/utils/icon_string_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    /*menuProvider.cargarData().then((value) => {
    print(value)
    });*/

    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapShot) {
        return ListView(
          children: _listaItems(snapShot.data, context),
        );
      },
    );
  }

  List<Widget> _listaItems(List data, BuildContext context) {
    print(data);

    final List<Widget> opciones = [];
    data.forEach((element) {
      final widgetTemp = ListTile(
        title: Text(element['texto']),
        leading: getIcon(element['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          //Navigator.push(context, MaterialPageRoute(builder: (context) => AlertPage()));
            Navigator.pushNamed(context, element['ruta']);
        },
      );
      opciones..add(widgetTemp)..add(Divider());
    }

    );

    return

      opciones

    ;
  }
}
