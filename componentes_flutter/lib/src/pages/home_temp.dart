import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp'),
      ),
      body: ListView(
          //children: _crearItems()),
          children: _crearListaCorta()),
    );
  }

  List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      lista..add(tempWidget)..add(Divider(height: 10.0));
    }
    return lista;
  }

  List<Widget> _crearListaCorta() {
    return opciones.map((e) {
      return Column(
        children: [
          ListTile(
            title: Text(e + '!'),
            subtitle: Text(e + 'sub-Titulo'),
            leading: Icon(Icons.access_alarm),
            trailing: Icon(Icons.arrow_back),
            onTap: (){},
          ),
          Divider()
        ],
      );
    }).toList();
  }
}
