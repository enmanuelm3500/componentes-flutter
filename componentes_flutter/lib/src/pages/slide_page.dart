import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SlidePage extends StatefulWidget {
  @override
  _SlidePageState createState() => _SlidePageState();
}

class _SlidePageState extends State<SlidePage> {
  double _valorSlider = 20.0;
  bool _checkValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slide'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: [
            _crearSlider(),
            _crearCheck(),
            _crearSwich(),
            Expanded(child: _crearImage())
          ],
        ),
      ),
    );
  }

  _crearSlider() {
    return Slider(
      activeColor: Colors.brown,
      label: 'Valor',
      divisions: 20,
      onChanged: (_checkValue)
          ? null
          : (valor) {
              setState(() {
                _valorSlider = valor;
              });
            },
      min: 10.0,
      max: 400.0,
      value: _valorSlider,
    );
  }

  _crearCheck() {
    return CheckboxListTile(
        value: _checkValue,
        title: Text('Bloquear Slide'),
        onChanged: (valor) {
          setState(() {
            _checkValue = valor;
          });
        });
  }

  _crearSwich() {
    return SwitchListTile(
        value: _checkValue,
        title: Text('Bloquear Slide'),
        onChanged: (valor) {
          setState(() {
            _checkValue = valor;
          });
        });
  }

  _crearImage() {
    return Image(
      image: NetworkImage(
          'https://i.blogs.es/f9a4f5/maxresdefault-1-/1366_2000.jpeg'),
      width: _valorSlider,
      fit: BoxFit.contain,
    );
  }
}
