import 'dart:async';

import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  ScrollController _scrollController = new ScrollController();

  List<int> _posicion = [1, 2, 3, 4, 5];
  int _ultimoItem = 0;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    agregar10();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        // agregar10();
        fetchData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('List Page'),
        ),
        body: Stack(
          children: [_createList(), _crearLoading()],
        ));
  }

  _createList() {
    return RefreshIndicator(
        child: ListView.builder(
            controller: _scrollController,
            itemCount: _posicion.length,
            itemBuilder: (BuildContext context, int index) {
              final image = _posicion[index];
              return FadeInImage(
                  placeholder: AssetImage('resources/img/jar-loading.gif'),
                  image: NetworkImage(
                      'https://picsum.photos/500/300/?image=$image'));
            }),
        onRefresh: obtenerPagina1);
  }

  void agregar10() {
    for (var i = 1; i < 10; i++) {
      _ultimoItem++;
      _posicion.add(_ultimoItem);
    }
    setState(() {});
  }

  Future fetchData() async {
    _isLoading = true;
    setState(() {
      final duration = new Duration(seconds: 2);
      return new Timer(duration, respuestaHTTP);
    });
  }

  void respuestaHTTP() {
    _isLoading = false;
    _scrollController.animateTo(_scrollController.position.pixels + 100,
        duration: Duration(milliseconds: 250), curve: Curves.fastOutSlowIn);
    agregar10();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.dispose();
  }

  _crearLoading() {
    if (_isLoading) {
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [CircularProgressIndicator()],
          ),
          SizedBox(height: 15.0)
        ],
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
      );
    } else {
      return Container();
    }
  }

  Future obtenerPagina1() async {
    final duration = new Duration(seconds: 2);
    new Timer(duration, () {
      _posicion.clear();
      _ultimoItem++;
      agregar10();
    });
    return Future.delayed(duration);
  }
}
