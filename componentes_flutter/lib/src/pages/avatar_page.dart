import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: [
          Container(
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://i.blogs.es/f9a4f5/maxresdefault-1-/1366_2000.jpeg'),
              radius: 25.0,
            ),
          ),
          Container(
              margin: EdgeInsets.only(right: 10.0),
              child: CircleAvatar(
                child: Text('SL'),
                backgroundColor: Colors.brown,
              ))
        ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://i.blogs.es/f9a4f5/maxresdefault-1-/1366_2000.jpeg'),
          placeholder: AssetImage('resources/img/jar-loading.gif'),
          fadeInDuration: Duration(milliseconds: 200),

        ),
      ),
    );
  }
}
