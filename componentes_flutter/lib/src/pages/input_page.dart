import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _password = '';
  String _date = '';
  String _optionSeleccionada = 'Volar';

  List<String> _poderes = ['Volar', 'Super Fuerza', 'Rayos X'];

  TextEditingController _inputFieldDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Input texto'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: [
          _crearInput(),
          Divider(),
          _crearCorreo(),
          Divider(),
          _crearPass(),
          Divider(),
          _crearDate(context),
          Divider(),
          _crearDropBox(),
        ],
      ),
    );
  }

  _crearInput() {
    return TextField(
      autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          counter: Text('${_nombre.length}/20'),
          hintText: 'Nombre de la persona',
          labelText: 'Nombre',
          helperText: 'Solo el nombre',
          suffixIcon: Icon(Icons.accessibility),
          icon: Icon(Icons.account_circle),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          )),
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
        });
      },
    );
  }

  _crearCorreo() {
    return TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            counter: Text('${_email.length}/20'),
            hintText: 'Correo de la persona',
            labelText: 'Correo',
            suffixIcon: Icon(Icons.alternate_email),
            icon: Icon(Icons.email),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
            )),
        onChanged: (valor) => setState(() {
              _email = valor;
            }));
  }

  _crearPass() {
    return TextField(
        obscureText: true,
        decoration: InputDecoration(
            hintText: 'Pass de la persona',
            labelText: 'Password',
            suffixIcon: Icon(Icons.lock_open),
            icon: Icon(Icons.lock),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
            )),
        onChanged: (valor) => setState(() {
              _password = valor;
            }));
  }

  _crearDate(BuildContext context) {
    return TextField(
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
          hintText: 'Fecha de nacimiento',
          labelText: 'Fecha de nacimiento',
          suffixIcon: Icon(Icons.perm_contact_calendar),
          icon: Icon(Icons.calendar_today),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          )),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime dateSelected = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2025));
    //locale: Locale('es','ES');

    if (dateSelected != null) {
      setState(() {
        _date = dateSelected.toString();
        _inputFieldDateController.text = _date;
      });
    }
  }

  List<DropdownMenuItem<dynamic>> getOptions() {
    List<DropdownMenuItem> lista = new List();
    _poderes.forEach((element) {
      lista.add(DropdownMenuItem(child: Text(element), value: element));
    });
    return lista;
  }

  _crearDropBox() {
    return Row(
      children: [
        Icon(Icons.select_all),
        SizedBox(width: 30.0),
        Expanded(
            child: DropdownButton(
                value: _optionSeleccionada,
                items: getOptions(),
                onChanged: (opt) {
                  setState(() {
                    _optionSeleccionada = opt;
                    print(_optionSeleccionada);
                  });
                }))
      ],
    );
  }
}
